﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports WindowsApplication10
Imports System.Security.Cryptography
Imports System.IO



<TestClass()> Public Class UnitTest1
    Function GetMD5(ByVal filePath As String)
        Dim md5 As MD5CryptoServiceProvider = New MD5CryptoServiceProvider
        Dim f As FileStream = New FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, 8192)

        f = New FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, 8192)
        md5.ComputeHash(f)
        f.Close()

        Dim hash As Byte() = md5.Hash
        Dim buff As StringBuilder = New StringBuilder
        Dim hashByte As Byte

        For Each hashByte In hash
            buff.Append(String.Format("{0:X2}", hashByte))
        Next

        Dim md5string As String
        md5string = buff.ToString()

        Return md5string

    End Function

    <TestMethod()> Public Sub TestMethod1()
        Assert.IsTrue(GetMD5("../../UnitTestsFiles/db.xml") = "61AEC344FBAB65A50D64A4E056D13B59")
        If System.IO.File.Exists("../../UnitTestsFiles/ExamBase.php") = True Then
            System.IO.File.Delete("../../UnitTestsFiles/ExamBase.php")
        End If
        Dim cd As New CreateDatabase
        cd.setFolderPath("../../UnitTestsFiles")
        cd.setFilePath("../../UnitTestsFiles/db.xml")
        cd.generate()
        Assert.IsTrue(GetMD5("../../UnitTestsFiles/ExamBase.php") = "B462CCE6FC06F3D94F8F312059457653")

    End Sub

    <TestMethod()> Public Sub TestMethod2()
        Dim filename As String = "../../UnitTestsFiles/db2.xml"

        Assert.IsTrue(GetMD5(filename) = "943F0D55B0711C3CA8BBD12EB5AFAD4B")

        'If System.IO.File.Exists("../../UnitTestsFiles/ExamBase.php") = True Then
        '    System.IO.File.Delete("../../UnitTestsFiles/ExamBase.php")
        'End If

        'If System.IO.File.Exists("../../UnitTestsFiles/CourseBase.php") = True Then
        '    System.IO.File.Delete("../../UnitTestsFiles/CourseBase.php")
        'End If

        'Dim cd As New CreateDatabase
        'cd.setFolderPath("../../UnitTestsFiles")
        'cd.setFilePath(filename)
        'cd.generate()
        ''Assert.IsTrue(GetMD5("../../UnitTestsFiles/CourseBase.php") = "943F0D55B0711C3CA8BBD12EB5AFAD4B")

    End Sub

End Class