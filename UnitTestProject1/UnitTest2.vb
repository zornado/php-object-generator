﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports WindowsApplication10
<TestClass()> Public Class UnitTest2

    <TestMethod()> Public Sub TestMTable()
        Dim aa As MTable
        aa = New MTable("exam", "Exam")
        Assert.IsTrue(aa.getDbTableName = "exam")
        Assert.IsTrue(aa.getCodeTableName = "Exam")


        aa = New MTable("exam")
        Assert.IsTrue(aa.getDbTableName = "exam")
        Assert.IsTrue(aa.getCodeTableName = "Exam")


        aa = New MTable()
        aa.setDbTableName("exam_is_beutiful")
        Assert.IsTrue(aa.getDbTableName = "exam_is_beutiful")
        Assert.IsTrue(aa.getCodeTableName = "ExamIsBeutiful")
    End Sub

End Class