﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnAdd = New System.Windows.Forms.Button()
        Me.LstTables = New System.Windows.Forms.ListBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.namea = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrimaryKey = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.unique = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.autoNumber = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.notNull = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tableName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Txtlist = New System.Windows.Forms.TextBox()
        Me.BtnRemove = New System.Windows.Forms.Button()
        Me.BtnUp = New System.Windows.Forms.Button()
        Me.BtnDwn = New System.Windows.Forms.Button()
        Me.TxtRel = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.BtnSave = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LstOneOne = New System.Windows.Forms.ListBox()
        Me.BtnOneOne = New System.Windows.Forms.Button()
        Me.BtnDelOneOne = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LstOneMany = New System.Windows.Forms.ListBox()
        Me.LstManyMany = New System.Windows.Forms.ListBox()
        Me.BtnOneMany = New System.Windows.Forms.Button()
        Me.BtnManyMany = New System.Windows.Forms.Button()
        Me.BtnDelOneMany = New System.Windows.Forms.Button()
        Me.BtnDelManyMany = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BtnAdd
        '
        Me.BtnAdd.Location = New System.Drawing.Point(12, 438)
        Me.BtnAdd.Name = "BtnAdd"
        Me.BtnAdd.Size = New System.Drawing.Size(75, 23)
        Me.BtnAdd.TabIndex = 0
        Me.BtnAdd.Text = "Add to list"
        Me.BtnAdd.UseVisualStyleBackColor = True
        '
        'LstTables
        '
        Me.LstTables.FormattingEnabled = True
        Me.LstTables.Location = New System.Drawing.Point(12, 12)
        Me.LstTables.Name = "LstTables"
        Me.LstTables.Size = New System.Drawing.Size(232, 394)
        Me.LstTables.TabIndex = 1
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(250, 470)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Generate"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(250, 12)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(650, 20)
        Me.TextBox1.TabIndex = 4
        Me.TextBox1.Text = "Exam"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(906, 12)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox2.Size = New System.Drawing.Size(354, 450)
        Me.TextBox2.TabIndex = 5
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.namea, Me.Type, Me.PrimaryKey, Me.unique, Me.autoNumber, Me.notNull, Me.tableName})
        Me.DataGridView1.Location = New System.Drawing.Point(250, 38)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(650, 424)
        Me.DataGridView1.TabIndex = 2
        '
        'namea
        '
        Me.namea.HeaderText = "Name"
        Me.namea.Name = "namea"
        '
        'Type
        '
        Me.Type.HeaderText = "Type"
        Me.Type.Name = "Type"
        '
        'PrimaryKey
        '
        Me.PrimaryKey.HeaderText = "PrimaryKey"
        Me.PrimaryKey.Name = "PrimaryKey"
        Me.PrimaryKey.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.PrimaryKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'unique
        '
        Me.unique.HeaderText = "unique"
        Me.unique.Name = "unique"
        Me.unique.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.unique.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'autoNumber
        '
        Me.autoNumber.HeaderText = "AutoNumber"
        Me.autoNumber.Name = "autoNumber"
        Me.autoNumber.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.autoNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'notNull
        '
        Me.notNull.HeaderText = "Not Null"
        Me.notNull.Name = "notNull"
        Me.notNull.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.notNull.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'tableName
        '
        Me.tableName.HeaderText = "TableName"
        Me.tableName.Name = "tableName"
        '
        'Txtlist
        '
        Me.Txtlist.Location = New System.Drawing.Point(12, 412)
        Me.Txtlist.Name = "Txtlist"
        Me.Txtlist.Size = New System.Drawing.Size(232, 20)
        Me.Txtlist.TabIndex = 6
        '
        'BtnRemove
        '
        Me.BtnRemove.Location = New System.Drawing.Point(184, 439)
        Me.BtnRemove.Name = "BtnRemove"
        Me.BtnRemove.Size = New System.Drawing.Size(60, 23)
        Me.BtnRemove.TabIndex = 7
        Me.BtnRemove.Text = "Remove from List"
        Me.BtnRemove.UseVisualStyleBackColor = True
        '
        'BtnUp
        '
        Me.BtnUp.Location = New System.Drawing.Point(93, 438)
        Me.BtnUp.Name = "BtnUp"
        Me.BtnUp.Size = New System.Drawing.Size(31, 23)
        Me.BtnUp.TabIndex = 8
        Me.BtnUp.Text = "Up"
        Me.BtnUp.UseVisualStyleBackColor = True
        '
        'BtnDwn
        '
        Me.BtnDwn.Location = New System.Drawing.Point(130, 438)
        Me.BtnDwn.Name = "BtnDwn"
        Me.BtnDwn.Size = New System.Drawing.Size(48, 23)
        Me.BtnDwn.TabIndex = 9
        Me.BtnDwn.Text = "Down"
        Me.BtnDwn.UseVisualStyleBackColor = True
        '
        'TxtRel
        '
        Me.TxtRel.Location = New System.Drawing.Point(12, 489)
        Me.TxtRel.Name = "TxtRel"
        Me.TxtRel.Size = New System.Drawing.Size(232, 20)
        Me.TxtRel.TabIndex = 12
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(437, 468)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(127, 23)
        Me.Button4.TabIndex = 13
        Me.Button4.Text = "Load Project"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'BtnSave
        '
        Me.BtnSave.Location = New System.Drawing.Point(331, 470)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Size = New System.Drawing.Size(100, 23)
        Me.BtnSave.TabIndex = 14
        Me.BtnSave.Text = "Save Project"
        Me.BtnSave.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 545)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Relationships"
        '
        'LstOneOne
        '
        Me.LstOneOne.FormattingEnabled = True
        Me.LstOneOne.Location = New System.Drawing.Point(12, 567)
        Me.LstOneOne.Name = "LstOneOne"
        Me.LstOneOne.Size = New System.Drawing.Size(232, 134)
        Me.LstOneOne.TabIndex = 16
        '
        'BtnOneOne
        '
        Me.BtnOneOne.Location = New System.Drawing.Point(12, 515)
        Me.BtnOneOne.Name = "BtnOneOne"
        Me.BtnOneOne.Size = New System.Drawing.Size(232, 27)
        Me.BtnOneOne.TabIndex = 17
        Me.BtnOneOne.Text = "One -- One"
        Me.BtnOneOne.UseVisualStyleBackColor = True
        '
        'BtnDelOneOne
        '
        Me.BtnDelOneOne.Location = New System.Drawing.Point(12, 707)
        Me.BtnDelOneOne.Name = "BtnDelOneOne"
        Me.BtnDelOneOne.Size = New System.Drawing.Size(232, 28)
        Me.BtnDelOneOne.TabIndex = 18
        Me.BtnDelOneOne.Text = "Delete Relationship"
        Me.BtnDelOneOne.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(42, 473)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(171, 13)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Table Field <contains> Table Field"
        '
        'LstOneMany
        '
        Me.LstOneMany.FormattingEnabled = True
        Me.LstOneMany.Location = New System.Drawing.Point(250, 567)
        Me.LstOneMany.Name = "LstOneMany"
        Me.LstOneMany.Size = New System.Drawing.Size(232, 134)
        Me.LstOneMany.TabIndex = 20
        '
        'LstManyMany
        '
        Me.LstManyMany.FormattingEnabled = True
        Me.LstManyMany.Location = New System.Drawing.Point(488, 567)
        Me.LstManyMany.Name = "LstManyMany"
        Me.LstManyMany.Size = New System.Drawing.Size(232, 134)
        Me.LstManyMany.TabIndex = 21
        '
        'BtnOneMany
        '
        Me.BtnOneMany.Location = New System.Drawing.Point(250, 515)
        Me.BtnOneMany.Name = "BtnOneMany"
        Me.BtnOneMany.Size = New System.Drawing.Size(232, 27)
        Me.BtnOneMany.TabIndex = 22
        Me.BtnOneMany.Text = "One --> Many"
        Me.BtnOneMany.UseVisualStyleBackColor = True
        '
        'BtnManyMany
        '
        Me.BtnManyMany.Location = New System.Drawing.Point(488, 515)
        Me.BtnManyMany.Name = "BtnManyMany"
        Me.BtnManyMany.Size = New System.Drawing.Size(232, 27)
        Me.BtnManyMany.TabIndex = 23
        Me.BtnManyMany.Text = "Many <--> Many"
        Me.BtnManyMany.UseVisualStyleBackColor = True
        '
        'BtnDelOneMany
        '
        Me.BtnDelOneMany.Location = New System.Drawing.Point(250, 707)
        Me.BtnDelOneMany.Name = "BtnDelOneMany"
        Me.BtnDelOneMany.Size = New System.Drawing.Size(232, 28)
        Me.BtnDelOneMany.TabIndex = 24
        Me.BtnDelOneMany.Text = "Delete Relationship"
        Me.BtnDelOneMany.UseVisualStyleBackColor = True
        '
        'BtnDelManyMany
        '
        Me.BtnDelManyMany.Location = New System.Drawing.Point(488, 707)
        Me.BtnDelManyMany.Name = "BtnDelManyMany"
        Me.BtnDelManyMany.Size = New System.Drawing.Size(232, 28)
        Me.BtnDelManyMany.TabIndex = 25
        Me.BtnDelManyMany.Text = "Delete Relationship"
        Me.BtnDelManyMany.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1272, 747)
        Me.Controls.Add(Me.BtnDelManyMany)
        Me.Controls.Add(Me.BtnDelOneMany)
        Me.Controls.Add(Me.BtnManyMany)
        Me.Controls.Add(Me.BtnOneMany)
        Me.Controls.Add(Me.LstManyMany)
        Me.Controls.Add(Me.LstOneMany)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.BtnDelOneOne)
        Me.Controls.Add(Me.BtnOneOne)
        Me.Controls.Add(Me.LstOneOne)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BtnSave)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.TxtRel)
        Me.Controls.Add(Me.BtnDwn)
        Me.Controls.Add(Me.BtnUp)
        Me.Controls.Add(Me.BtnRemove)
        Me.Controls.Add(Me.Txtlist)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.LstTables)
        Me.Controls.Add(Me.BtnAdd)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BtnAdd As System.Windows.Forms.Button
    Friend WithEvents LstTables As System.Windows.Forms.ListBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents namea As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrimaryKey As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents unique As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents autoNumber As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents notNull As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Txtlist As System.Windows.Forms.TextBox
    Friend WithEvents BtnRemove As System.Windows.Forms.Button
    Friend WithEvents BtnUp As System.Windows.Forms.Button
    Friend WithEvents BtnDwn As System.Windows.Forms.Button
    Friend WithEvents TxtRel As System.Windows.Forms.TextBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents BtnSave As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LstOneOne As System.Windows.Forms.ListBox
    Friend WithEvents BtnOneOne As System.Windows.Forms.Button
    Friend WithEvents BtnDelOneOne As System.Windows.Forms.Button
    Friend WithEvents tableName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LstOneMany As System.Windows.Forms.ListBox
    Friend WithEvents LstManyMany As System.Windows.Forms.ListBox
    Friend WithEvents BtnOneMany As System.Windows.Forms.Button
    Friend WithEvents BtnManyMany As System.Windows.Forms.Button
    Friend WithEvents BtnDelOneMany As System.Windows.Forms.Button
    Friend WithEvents BtnDelManyMany As System.Windows.Forms.Button

End Class
