﻿Option Explicit On
Option Strict On
Imports System.Text
Imports System.Xml

Public Class CreateDatabase
    'M-S Must have one and only one primary key for each table
    Private rows As New List(Of DatabaseRow)
    Private tableList As New List(Of MTable) ' list of php tablename
    Private sb As StringBuilder
    Private folderPath As String
    Private filePath As String
    Private oneToOneRel As OneToOne
    Public Class CreateDatabaseCapsule
        Public rows As New List(Of DatabaseRow)
        Public tableList As New List(Of MTable)
        Public sb As StringBuilder
        Public folderPath As String
        Public filePath As String
        Public oneToOneRel As OneToOne
    End Class
    Public Function capsulate() As CreateDatabaseCapsule
        Dim aa As New CreateDatabaseCapsule
        aa.rows = Me.rows
        aa.tableList = Me.tableList
        aa.sb = Me.sb
        aa.folderPath = Me.folderPath
        aa.filePath = Me.filePath
        aa.oneToOneRel = Me.oneToOneRel
        Return aa
    End Function
    Public Sub New()
        Me.tableList.Add(New MTable("exam"))
    End Sub
    Public Sub setFilePath(ByVal _filePath As String)
        Me.filePath = _filePath
    End Sub
    Public Sub setFolderPath(ByVal _folderPath As String)
        Me.folderPath = _folderPath
    End Sub
    Public Sub addRow(ByVal cdbr As DatabaseRow)
        cdbr.checkRowIsConsistent()
        rows.Add(cdbr)
    End Sub
    Private Function getHeader(currentTableName As MTable) As String
        Dim sa As New StringBuilder
        sa.AppendLine("<?php")
        sa.Append("abstract class ") : sa.Append(currentTableName.getCodeTableName).AppendLine("Base {")
        tabs(0) : sa.AppendLine("protected $msqlserver;")
        tabs(0) : sa.AppendLine("public function setMSqlServer($_msqlserver){")
        tabs(1) : sa.AppendLine("$this->msqlserver = $_msqlserver;")
        tabs(0) : sa.AppendLine("}")
        tabs(0) : sa.AppendLine("public function getMSqlServer(){")
        tabs(1) : sa.AppendLine("return $this->msqlserver;")
        tabs(0) : sa.AppendLine("}")
        tabs(0) : sa.AppendLine("protected $modifyRecord;")
        sa.Append(Chr(9)) : sa.AppendLine("public function setModifyRecord($_modifyRecord) {")
        sa.Append(Chr(9)) : sa.Append(Chr(9)) : sa.AppendLine("$this->modifyRecord = $_modifyRecord;")
        sa.Append(Chr(9)) : sa.AppendLine("}")

        sa.Append(Chr(9)) : sa.AppendLine("public function getModifyRecord() {")
        sa.Append(Chr(9)) : sa.Append(Chr(9)) : sa.AppendLine("return $this->modifyRecord;")
        sa.Append(Chr(9)) : sa.AppendLine("}")
        Return sa.ToString
    End Function
    Private Function getAdtConstructor(ByVal tableName As MTable) As String
        Dim sa As New StringBuilder
        tabs(0) : sa.AppendLine("public function __construct() {")
        For Each ro As DatabaseRow In rows
            If ro.tableName = tableName.getDbTableName Then
                sa.Append("$this->allow_get_").Append(ro.namea).AppendLine(" = true;")
                sa.Append("$this->allow_set_").Append(ro.namea).AppendLine(" = true;")
            End If
        Next
        tabs(1) : sa.AppendLine("$this->modifyRecord = false;")
        tabs(0) : sa.AppendLine("}")
        Return sa.ToString
    End Function
    Private Function getAdtCapsulator(ByVal tableNm As MTable) As String
        Dim sa As New StringBuilder
        sa.AppendLine("<?php")
        sa.Append("class ") : sa.Append(tableNm.getCodeTableName).AppendLine("BaseCapsule {")
        For Each ro As DatabaseRow In rows
            If ro.tableName = tableNm.getDbTableName Then
                sa.Append("public $allow_get_").Append(ro.namea).AppendLine(";")
                sa.Append("public $allow_set_").Append(ro.namea).AppendLine(";")
                sa.Append("public $").Append(ro.namea).AppendLine(";")
            End If
        Next
        sa.AppendLine("}?>")
        Return sa.ToString
    End Function
    Private Function getFunctionCapsulator(ByVal tableName As MTable) As String
        Dim sa As New StringBuilder
        Dim obj As String = New StringBuilder().Append("$" + tableName.getCodeTableName + "Obj").ToString
        sa.AppendLine("public function capsulate(){")
        sa.Append(obj + " = new ").Append(tableName.getCodeTableName).AppendLine("BaseCapsule();")
        For Each ro As DatabaseRow In rows
            If ro.tableName = tableName.getDbTableName Then
                sa.Append(obj + "->allow_get_").Append(ro.namea).Append(" = $this->allow_get_").Append(ro.namea).AppendLine(";")
                sa.Append(obj + "->allow_set_").Append(ro.namea).Append(" = $this->allow_set_").Append(ro.namea).AppendLine(";")
                sa.Append(obj + "->").Append(ro.namea).Append(" = $this->").Append(ro.namea).AppendLine(";")
            End If
        Next
        sa.AppendLine("return " + obj + ";")
        sa.AppendLine("}")
        Return sa.ToString
    End Function
    Private Function getAdtFooter(ByVal tableName As MTable) As String
        Dim sa As New StringBuilder
        sa.Append("}?>")
        Return sa.ToString
    End Function
    Public Sub generate()
        Try
            Me.LoadDataFromXML()
            sb = New StringBuilder
            For Each table As MTable In Me.tableList
                sb.Append(getHeader(table))
                sb.Append(getAdtConstructor(table))
                sb.Append(getFunctionCapsulator(table))

                Dim dbrowlist As New List(Of DatabaseRow)

                For Each dbrow In Me.rows
                    If dbrow.tableName = table.getDbTableName Then
                        dbrowlist.Add(dbrow)
                    End If
                Next

                For Each dbrow In Me.rows
                    If dbrow.tableName = table.getDbTableName Then
                        sb.AppendLine(dbrow.getDeclartion)


                        sb.Append(Chr(9)) : sb.Append("public function delete") : sb.Append(dbrow.getFieldPhpName) : sb.Append("($_") : sb.Append(dbrow.namea) : sb.AppendLine(") {")
                        generateDeleteStatement(table.getCodeTableName, dbrow.namea)
                        sb.Append(Chr(9)) : sb.AppendLine("}")

                        sb.Append(Chr(9)) : sb.Append("public function update") : sb.Append(dbrow.getFieldPhpName) : sb.Append("($_") : sb.Append(dbrow.namea) : sb.AppendLine(") {")
                        generateDeleteStatement(table.getCodeTableName, dbrow.namea)
                        sb.Append(Chr(9)) : sb.AppendLine("}")

                        sb.Append(Chr(9)) : sb.Append("public function fetch") : sb.Append(dbrow.getFieldPhpName) : sb.Append("($_") : sb.Append(dbrow.namea) : sb.AppendLine(") {")
                        sb.Append(Chr(9)) : sb.Append("return $this->fetch(""" + dbrow.namea + " = ?"" , array(&").Append("$_") : sb.Append(dbrow.namea) : sb.AppendLine("));")
                        sb.Append(Chr(9)) : sb.AppendLine("}")



                    End If
                Next

                sb.Append(Chr(9)) : sb.Append("public function fetch($sqlss, $parameterList) {")
                generateFetchStatement(table.getCodeTableName, dbrowlist)
                sb.Append(Chr(9)) : sb.AppendLine("}")

                sb.Append(Chr(9)) : sb.AppendLine("public function create" + table.getCodeTableName + "($dropIfExists) {")
                tabs(1) : sb.AppendLine("if ($dropIfExists) {")
                tabs(2) : sb.AppendLine("$sql = 'DROP TABLE " + dbrowlist(0).tableName + "';")
                tabs(2) : sb.AppendLine("$this->msqlserver->excuteSql($sql);")
                tabs(1) : sb.AppendLine("}")
                generateCreateTableSql(table.getCodeTableName, dbrowlist)
                sb.Append(Chr(9)) : sb.AppendLine("}")

                sb.Append(Chr(9)) : sb.AppendLine("public function truncate" + table.getCodeTableName + "() {")
                tabs(1) : sb.Append("$sql = 'TRUNCATE TABLE ") : sb.Append(dbrowlist(0).tableName) : sb.AppendLine(";';")
                tabs(1) : sb.AppendLine("return $this->msqlserver->excuteSql($sql);")
                sb.Append(Chr(9)) : sb.AppendLine("}")

                sb.Append(Chr(9)) : sb.AppendLine("public function deleteAll() {")
                tabs(1) : sb.Append("$sql = 'DELETE FROM ") : sb.Append(dbrowlist(0).tableName) : sb.AppendLine(";';")
                tabs(1) : sb.AppendLine("return $this->msqlserver->excuteSql($sql);")
                sb.Append(Chr(9)) : sb.AppendLine("}")


                generateInsertFunction(table.getCodeTableName, dbrowlist)
                generateUpdateTableFunction(table.getCodeTableName, dbrowlist)


                sb.Append(Chr(9)) : sb.AppendLine("public function save" + table.getCodeTableName + "() {")
                tabs(1) : sb.AppendLine("if ($this->modifyRecord){")
                tabs(2) : sb.AppendLine("return $this->update" + table.getCodeTableName + "();")
                tabs(1) : sb.AppendLine("} else {")
                tabs(2) : sb.AppendLine("return $this->insert" + table.getCodeTableName + "();")
                tabs(1) : sb.AppendLine("}")
                sb.Append(Chr(9)) : sb.AppendLine("}")

                sb.Append(getAdtFooter(table))
                sb.Append(getAdtCapsulator(table))


                writeFile(sb.ToString, table.getCodeTableName + "Base.php")
            Next
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try


    End Sub
    Public Sub LoadDataFromXML()
        Dim xr As XmlReader = XmlReader.Create(Me.filePath)
        Dim aa As New List(Of DatabaseRow)

        Dim dr As DatabaseRow = Nothing
        Dim currentTableName As String = Nothing
        Do While xr.Read()
            If xr.NodeType = XmlNodeType.Element AndAlso xr.Name = "field" Then
                dr = New DatabaseRow

                dr.namea = xr.GetAttribute("name")
                dr.type = xr.GetAttribute("type")
                dr.primaryKey = stringToBool(xr, "primary")
                dr.autoNumber = stringToBool(xr, "autoNumber")
                dr.notNull = stringToBool(xr, "notNull")
                dr.tableName = currentTableName
                dr.checkRowIsConsistent()
                rows.Add(dr)
                dr.getDeclartion()
            ElseIf xr.NodeType = XmlNodeType.Element AndAlso xr.Name = "table" Then
                currentTableName = xr.GetAttribute("name")
            End If
        Loop
    End Sub
    Private Function stringToBool(ByRef xr As XmlReader, ByRef attr As String) As Boolean
        If xr.GetAttribute(attr) IsNot Nothing Then
            If xr.GetAttribute(attr).ToLower = "true".ToLower Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
        Return Nothing
    End Function
    Private Sub tabs(count As Integer)
        For i As Integer = 0 To count
            sb.Append(Chr(9))
        Next
    End Sub
    Private Sub writeFile(ByRef text As String, currentTable As String)
        Try


            Dim fileName As String = Me.folderPath & "\" & currentTable

            If (Not System.IO.Directory.Exists(Me.folderPath)) Then
                System.IO.Directory.CreateDirectory(Me.folderPath)
            End If

            If System.IO.File.Exists(fileName) = False Then
                IO.File.Create(fileName).Dispose()
            End If

            Dim writer As New System.IO.StreamWriter(fileName)
            writer.Write(text.ToString)
            writer.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub generateDeleteStatement(ByVal tableName As String, ByVal colname As String)
        tabs(1) : sb.Append("$sql = 'DELETE FROM ") : sb.Append(tableName.ToLower) : sb.Append(" WHERE ") : sb.Append(colname) : sb.AppendLine(" = ?';")
        tabs(1) : sb.Append("$parameterList = array(") : sb.Append("&$_") : sb.Append(colname) : sb.AppendLine(");")
        tabs(1) : sb.AppendLine("return $this->msqlserver->excuteStatement($sql, $parameterList);")
    End Sub
    Private Sub generateFetchStatement(ByVal tableName As String, currentRows As List(Of DatabaseRow))
        tabs(1) : sb.Append("$sql = ""SELECT * FROM ") : sb.Append(tableName.ToLower) : sb.AppendLine(" WHERE $sqlss"";")
        tabs(1) : sb.AppendLine("$arr = $this->msqlserver->queryStatement($sql, $parameterList);")
        tabs(1) : sb.Append("$").Append(tableName.ToLower).AppendLine("Vector = array();")
        tabs(1) : sb.AppendLine("for ($i = 0; $i < count ($arr); $i++){")
        tabs(2) : sb.Append("$_").Append(tableName.ToLower).Append(" = new ").Append(currentRows(0).getTablePhpName).AppendLine("();")
        For i As Integer = 0 To currentRows.Count - 1
            tabs(2) : sb.Append("$_").Append(tableName.ToLower).Append("->set").Append(currentRows(i).getFieldPhpName).Append("(").Append("$arr[$i]['").Append(currentRows(i).namea.ToLower).AppendLine("']);")
        Next
        tabs(2) : sb.Append("$_").Append(tableName.ToLower).AppendLine("->setMSqlServer($this->msqlserver);")
        tabs(2) : sb.Append("$_").Append(tableName.ToLower).AppendLine("->setModifyRecord(true);")

        tabs(2) : sb.Append("$").Append(tableName.ToLower).Append("Vector[] = ").Append("$_").Append(tableName.ToLower).AppendLine(";")
        tabs(1) : sb.AppendLine("}")
        tabs(1) : sb.Append("return ").Append("$").Append(tableName.ToLower).AppendLine("Vector;")
    End Sub
    Private Sub generateUpdateTableFunction(ByVal tableName As String, currentRows As List(Of DatabaseRow))
        'sb = New StringBuilder
        sb.Append(Chr(9)) : sb.AppendLine("public function update" + tableName + "() {")
        tabs(1) : sb.Append("$sql = 'UPDATE ") : sb.Append(tableName.ToLower) : sb.Append(" SET ")
        For i As Integer = 0 To currentRows.Count - 1
            If currentRows(i).primaryKey = False Then
                sb.Append(currentRows(i).namea.ToLower) : sb.Append(" = ?")
            End If
            If i <> currentRows.Count - 1 And currentRows(i).primaryKey = False Then
                sb.Append(", ")
            End If
        Next
        sb.Append(" WHERE ")
        For i As Integer = 0 To currentRows.Count - 1
            If currentRows(i).primaryKey = True Then
                sb.Append(currentRows(i).namea.ToLower) : sb.Append(" = ?")
            End If
        Next
        sb.AppendLine(";';")

        tabs(1) : sb.AppendLine("$parameterList = array(")

        For i As Integer = 0 To currentRows.Count - 1
            If currentRows(i).primaryKey = False Then
                tabs(1) : sb.Append("&$this->") : sb.Append(currentRows(i).namea.ToLower)
            End If
            If i <> 0 Then
                sb.AppendLine(", ")
            End If
        Next

        For i As Integer = 0 To currentRows.Count - 1
            If currentRows(i).primaryKey = True Then
                tabs(1) : sb.Append("&$this->") : sb.AppendLine(currentRows(i).namea.ToLower)
            End If
        Next

        tabs(1) : sb.AppendLine(");")
        tabs(1) : sb.AppendLine("return $this->msqlserver->excuteStatement($sql, $parameterList);")
        sb.Append(Chr(9)) : sb.AppendLine("}")
    End Sub
    Private Sub generateInsertFunction(ByVal tableName As String, currentRows As List(Of DatabaseRow))
        sb.Append(Chr(9)) : sb.AppendLine("public function insert" + tableName + "() {")
        tabs(1) : sb.Append("$sql = 'INSERT INTO ") : sb.Append(tableName.ToLower) : sb.Append(" (")
        Dim sa As New StringBuilder

        For i As Integer = 0 To currentRows.Count - 1
            If currentRows(i).autoNumber = False Then
                sb.Append(currentRows(i).namea.ToLower)
            End If
            If i <> currentRows.Count - 1 And currentRows(i).autoNumber = False Then
                sb.Append(", ")
            End If
        Next

        sb.Append(") VALUES (")

        For i As Integer = 0 To currentRows.Count - 1
            If currentRows(i).autoNumber = False Then
                sb.Append("?")
            End If
            If i <> currentRows.Count - 1 And currentRows(i).autoNumber = False Then
                sb.Append(", ")
            End If
        Next
        sb.AppendLine(");';")

        tabs(1) : sb.AppendLine("$parameterList = array(")
        For i As Integer = 0 To currentRows.Count - 1
            If currentRows(i).autoNumber = False Then
                tabs(1) : sb.Append("&$this->") : sb.Append(currentRows(i).namea.ToLower)
            End If
            If i <> currentRows.Count - 1 And currentRows(i).autoNumber = False Then
                sb.AppendLine(", ")
            End If
        Next
        tabs(1) : sb.AppendLine(");")
        tabs(1) : sb.AppendLine("return $this->msqlserver->excuteStatement($sql, $parameterList);")
        sb.Append(Chr(9)) : sb.AppendLine("}")

    End Sub

    Private Sub generateCreateTableSql(ByVal tableName As String, currentRows As List(Of DatabaseRow))
        tabs(2) : sb.Append("$sql = 'CREATE TABLE ") : sb.Append(tableName.ToLower) : sb.AppendLine(" (")
        For i As Integer = 0 To currentRows.Count - 1
            Dim currentRow As DatabaseRow = currentRows(i)

            tabs(2) : sb.Append(currentRow.namea.ToLower) : sb.Append(" ") : sb.Append(currentRow.type.ToUpper)

            If currentRows(i).notNull = True And currentRows(i).primaryKey = True And currentRows(i).autoNumber = True Then
                sb.Append(" IDENTITY(1,1) PRIMARY KEY")
            ElseIf currentRows(i).notNull = True And currentRows(i).primaryKey = True Then
                sb.Append(" PRIMARY KEY")
            ElseIf currentRows(i).notNull = True Then
                sb.Append(" NOT NULL")
            End If
            If currentRows(i).autoNumber = True Then

            End If

            'M-S append a comma after each field
            If i <> currentRows.Count - 1 Then
                sb.AppendLine(",")
                'M-S if reached to the end just add empty line
            ElseIf i = currentRows.Count - 1 Then
                sb.AppendLine()
            End If

        Next
        tabs(2) : sb.AppendLine(");';")
        tabs(1) : sb.AppendLine("return $this->msqlserver->excuteSql($sql);")
    End Sub
End Class
