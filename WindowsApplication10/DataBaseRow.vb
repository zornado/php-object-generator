﻿Option Explicit On
Option Strict On
Public Class DatabaseRow
    Public Property namea As String
    Public Property type As String
    Public Property primaryKey As Boolean
    Public Property unique As Boolean
    Public Property autoNumber As Boolean
    Public Property notNull As Boolean
    Public Property tableName As String
    Public Property phpFieldName As String
    Public Property phpTableName As String
   
    Public Sub New()
        Me.notNull = True
    End Sub

    Public Sub checkRowIsConsistent()
        If primaryKey = True And notNull = False Then
            Throw New Exception("M-S Row data is not consistent")
        End If
        If tableName Is Nothing Or tableName = "" Then
            Throw New Exception("M-S Table Name is not found")
        End If
    End Sub

    Public Function getTablePhpName() As String
        If Me.phpTableName = "" Or Me.phpTableName Is Nothing Then
            Return camelCase(Me.tableName)
        End If
        Return Nothing
    End Function

    Public Function getFieldPhpName() As String
        If Me.phpFieldName = "" Or Me.phpFieldName Is Nothing Then
            Return camelCase(Me.namea)
        End If
        Return Nothing
    End Function
    Private Function camelCase(text As String) As String
        Dim strArr() As String
        strArr = text.Split(System.Convert.ToChar("_"))
        Dim sa As New System.Text.StringBuilder
        For i As Integer = 0 To strArr.Length - 1
            sa.Append(UppercaseFirstLetter(strArr(i)))
        Next
        Return sa.ToString
    End Function
    Private Function UppercaseFirstLetter(ByVal val As String) As String
        ' Test for nothing or empty.
        If String.IsNullOrEmpty(val) Then
            Return val
        End If

        ' Convert to character array.
        Dim array() As Char = val.ToCharArray

        ' Uppercase first character.
        array(0) = Char.ToUpper(array(0))

        ' Return new string.
        Return New String(array)
    End Function

    Public Function getDeclartion() As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("protected $").Append(Me.namea).AppendLine(";")
        sb.Append("protected $allow_set_").Append(Me.namea).AppendLine(";")
        sb.Append("protected $allow_get_").Append(Me.namea).AppendLine(";")
        sb.Append("public function set") : sb.Append(Me.getFieldPhpName) : sb.Append("($_") : sb.Append(Me.namea) : sb.AppendLine(") {")
        sb.Append("if ($this->allow_set_").Append(Me.namea).AppendLine("){")
        sb.Append("$this->") : sb.Append(Me.namea) : sb.Append(" = ") : sb.Append("$_") : sb.Append(Me.namea) : sb.AppendLine(";")
        sb.AppendLine("}")
        sb.AppendLine("}")
        sb.Append("public function get") : sb.Append(Me.getFieldPhpName) : sb.AppendLine("() {")
        sb.Append("if ($this->allow_get_").Append(Me.namea).AppendLine("){")


        sb.Append("return $this->") : sb.Append(Me.namea) : sb.AppendLine(";").AppendLine("}")
        sb.AppendLine("}")
        sb.Append("public function allow").Append(Me.getFieldPhpName).AppendLine("Setter($val){")
        sb.Append("$this->allow_set_").Append(Me.namea).AppendLine(" = $val;")
        sb.AppendLine("}")
        sb.Append("public function allow").Append(Me.getFieldPhpName).AppendLine("Getter($val){")
        sb.Append("$this->allow_get_").Append(Me.namea).AppendLine(" = $val;")
        sb.AppendLine("}")
        Return sb.ToString
    End Function

End Class
