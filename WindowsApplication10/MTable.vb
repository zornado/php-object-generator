﻿Option Explicit On
Option Strict On
Public Class MTable
    Private dbTableName As String
    Private codeTableName As String


    Public Sub setDbTableName(val As String)
        Me.dbTableName = val
    End Sub
    Public Sub setCodeTableName(val As String)
        Me.codeTableName = val
    End Sub

    Public Function getDbTableName() As String
        If Me.dbTableName Is Nothing Or Me.dbTableName = "" Then
            Return camelCase(dbTableName)
        Else
            Return Me.dbTableName
        End If
        Return Nothing
    End Function
    Public Function getCodeTableName() As String
        If Me.codeTableName Is Nothing Or Me.codeTableName = "" Then
            Return camelCase(Me.dbTableName)
        Else
            Return Me.codeTableName
        End If
        Return Nothing
    End Function
    Public Sub New()
    End Sub
    Public Sub New(val1 As String)
        Me.dbTableName = val1
    End Sub
    Public Sub New(val1 As String, val2 As String)
        Me.dbTableName = val1
        Me.codeTableName = val2
    End Sub
    Private Function camelCase(text As String) As String
        Dim strArr() As String
        strArr = text.Split(System.Convert.ToChar("_"))
        Dim sa As New System.Text.StringBuilder
        For i As Integer = 0 To strArr.Length - 1
            sa.Append(UppercaseFirstLetter(strArr(i)))
        Next
        Return sa.ToString
    End Function
    Private Function UppercaseFirstLetter(ByVal val As String) As String
        ' Test for nothing or empty.
        If String.IsNullOrEmpty(val) Then
            Return val
        End If

        ' Convert to character array.
        Dim array() As Char = val.ToCharArray

        ' Uppercase first character.
        array(0) = Char.ToUpper(array(0))

        ' Return new string.
        Return New String(array)
    End Function
End Class
