﻿Option Explicit On
Option Strict On
Public Class Relation
    Public Property tableLeft As String
    Public Property fieldLeft As String
    Public Property tableRight As String
    Public Property fieldRight As String
    Public Sub New(ByVal _tableLeft As String, ByVal _fieldLeft As String, ByVal _tableRight As String, ByVal _fieldRight As String)
        Me.tableLeft = _tableLeft
        Me.fieldLeft = _fieldLeft
        Me.tableRight = _tableRight
        Me.fieldRight = _fieldRight
    End Sub

    Public Function getOtherTable(ByVal currentTable As String) As String
        Dim otherTable As String = Nothing
        If currentTable = Me.tableRight Then
            otherTable = Me.tableLeft
        ElseIf currentTable = Me.tableLeft Then
            otherTable = Me.tableRight
        End If
        Return otherTable
    End Function
End Class
