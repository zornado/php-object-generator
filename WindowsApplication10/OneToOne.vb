﻿Option Strict On
Option Explicit On
Public Class OneToOne
    Private relations As New List(Of Relation)
    Public Sub addToRelations(ByRef r As Relation)
        relations.Add(r)
    End Sub
    Public Function getRelations() As List(Of Relation)
        Return Me.relations
    End Function
End Class
